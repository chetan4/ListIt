#!/usr/bin/env ruby
require 'rubygems'
require 'bundler/setup'
require 'mailman'

Mailman.config.logger = Logger.new("log/mailman.log")
Mailman.config.pop3 = {
  server: 'pop.gmail.com', port: 995, ssl: true,
  username: 'post@firelist.it',
  password: 'idyllicsoftware'
}                     

Mailman.config.poll_interval = 60

Mailman::Application.run do
  default do                                 
    begin          
     Resque.enqueue(MailmanWorker,message.to_s)
    rescue => e
      Mailman.logger.error "Exception occurred while receiving message:\n #{message}"
      Mailman.logger.error "#{e.backtrace}"
    end
  end
end
