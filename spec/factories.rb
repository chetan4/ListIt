FactoryGirl.define do
  factory :user do
    sequence :email do |i| 
      "testuser#{i}@list.it"
    end
    sequence :name do 
      |i| "test user#{i}"
    end
    password_salt "passwordsalt"
    perishable_token "windowintheskies"
    single_access_token "123123123"
  
  end  

  factory :tag do
    keywords "rails,fulltext,solr"
  end  
  
  factory :list do
    title "Sunspot Solr List"
    description "Contains documentation from multiple sources on how to use Solr to how tweak it for optimal performance."
    association :creator, factory: :user
    association :tag, factory: :tag
  end

  factory :url do
    title "An Open Letter To Those Not Employed At Instagram | TechCrunch"
    url "http://techcrunch.com/2012/04/15/an-open-letter-to-those-not-employed-at-instagram/"
    description "Dear Non-Instagramers, Sorry that you didnt get bought out for $1 billion last week.Kevin Systrom just made enough money to buy a boat big enough"
    type "Url"
    association :list, factory: :list
    image_url "http://tctechcrunch2011.files.wordpress.com/2012/04/instagram.png?w=150"
    association :creator, factory: :user    
  end   
  
  factory :rating do
    association :list, factory: :list
    association :user, factory: :user
    rate 1
  end
end
