require 'spec_helper'

describe "list_items/new" do
  before(:each) do
    assign(:list_item, stub_model(ListItem,
      :title => "MyString",
      :url => "MyString",
      :description => "MyText",
      :list_id => 1,
      :owner_id => 1
    ).as_new_record)
  end

  it "renders new list_item form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => list_items_path, :method => "post" do
      assert_select "input#list_item_title", :name => "list_item[title]"
      assert_select "input#list_item_url", :name => "list_item[url]"
      assert_select "textarea#list_item_description", :name => "list_item[description]"
      assert_select "input#list_item_list_id", :name => "list_item[list_id]"
      assert_select "input#list_item_owner_id", :name => "list_item[owner_id]"
    end
  end
end
