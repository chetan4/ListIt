require 'spec_helper'

describe "list_items/index" do
  before(:each) do
    assign(:list_items, [
      stub_model(ListItem,
        :title => "Title",
        :url => "Url",
        :description => "MyText",
        :list_id => 1,
        :owner_id => 2
      ),
      stub_model(ListItem,
        :title => "Title",
        :url => "Url",
        :description => "MyText",
        :list_id => 1,
        :owner_id => 2
      )
    ])
  end

  it "renders a list of list_items" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Url".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
