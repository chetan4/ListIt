require 'spec_helper'

describe "list_items/show" do
  before(:each) do
    @list_item = assign(:list_item, stub_model(ListItem,
      :title => "Title",
      :url => "Url",
      :description => "MyText",
      :list_id => 1,
      :owner_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Title/)
    rendered.should match(/Url/)
    rendered.should match(/MyText/)
    rendered.should match(/1/)
    rendered.should match(/2/)
  end
end
