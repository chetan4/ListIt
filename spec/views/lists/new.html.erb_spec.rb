require 'spec_helper'

describe "lists/new" do
  before(:each) do
    assign(:list, stub_model(List,
      :title => "MyString",
      :owner_id => 1,
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new list form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => lists_path, :method => "post" do
      assert_select "input#list_title", :name => "list[title]"
      assert_select "input#list_owner_id", :name => "list[owner_id]"
      assert_select "textarea#list_description", :name => "list[description]"
    end
  end
end
