require 'spec_helper'

describe "URL" do
  before(:each) do
    @user = FactoryGirl.create(:user, :password => 'testtesttest', :password_confirmation => 'testtesttest')
    @list = FactoryGirl.create(:list, :creator => @user)
    @url = FactoryGirl.build(:url, :creator => @user, :list => @list)
  end
  
  context "Creation" do
    it "should have error on url if url is not present" do
      @url.url = ''
      @url.should_not be_valid
      @url.should have(1).error_on(:url)      
    end 
    
    it "should have error on url if the format of url is incorrect" do
      @url.url = "wtta://tctechcrunch2011.files.wordpress.com/2012/04/instagram.png?w=150"
      @url.should_not be_valid
      @url.should have(1).error_on(:url)
    end
  end
  
end
