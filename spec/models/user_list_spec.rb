require 'spec_helper'

describe UserList do
  before(:each) do
    @user = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc')
    @user.activate!

    @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
    :description => "Dear Non-Instagramers, Sorry that you didnt get bought out for $1 billion last week.Kevin Systrom just made enough money to buy a boat big enough",
    :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))
    
    @list.create_unmoderated_membership(@user)
  end
  
  it "should list all membership requests made to a given list " do
    @user_2 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc', :single_access_token => "12322112322")
    @user_2.activate!
    @list.send(:create_membership_request, @user_2)
    UserList.requested(@user).should_not be_empty        
    UserList.requested(@user).first.should be_instance_of(UserList)
    UserList.requested(@user).first.user.should == @user_2
  end
  
end
