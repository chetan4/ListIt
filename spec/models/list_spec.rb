require 'spec_helper'

describe List do 
  before(:each) do
    @user = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc')
    @user.activate!    
    @user_2 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc', :single_access_token => '12221222121')
   @user_2.activate! 
  end              
  
  context "Create list without description" do
    it "should have 1 error on description" do
      @list = FactoryGirl.build(:list, :description => '', :creator => @user)
      @list.save
      @list.should_not be_valid
      @list.should have(1).error_on(:description)
    end    
  end
  
  context "Create a list without title" do
    it "should have 1 error on title" do
      @list = FactoryGirl.build(:list, :title => '', :creator => @user)      
      @list.save                                
      @list.should_not be_valid
      @list.should have(1).error_on(:title)
    end         
  end
  
  context "It should be associated to a user" do
    it "should belong to a user" do
      @list = FactoryGirl.build(:list, :creator => @user)      
      @list.creator.should_not be_nil
    end
  end 
  
  context "Search methods _list_tags_" do
    it "should display all tags without commas and new line characters" do
      @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
      :description => "Dear Non-Instagramers, Sorry that you didnt get bought out for $1 billion last week.Kevin Systrom just made enough money to buy a boat big enough",
      :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))

      @list.list_tags.should == "instagram startups entrepreneurs"
    end
  end
  
  context "Search methods _url_description_" do
    it "should display all description of all urls within a list " do
      @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
      :description => "Dear Non-Instagramers", :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))  
            
      @url = FactoryGirl.create(:url, :creator => @user, :list => @list, :title => "Instagram members",
                                :url => 'http://techcrunch.com/2012/04/15/an-open-letter-to-those-not-employed-at-instagram/',
        :description => "Dear Non-Instagramers, Sorry that you didnt get bought out for $1 billion last week.Kevin Systrom just made enough money to buy a boat big enough")
      @list.urls.push(@url)      
      @list.url_description.should == "Dear Non-Instagramers Sorry that you didnt get bought out for $1 billion last week.Kevin Systrom just made enough money to buy a boat big enough"        
    end
  end
  
  context "Memberships" do
    it "should create a new membership for unmoderated lists (create_unmoderated_memberships)" do 
             
      @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
      :description => "Dear Non-Instagramers", :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))
      @list.moderated = false
      @list.save             
      @list.users.push(@user)
      @list.create_unmoderated_membership(@user)
      @list.users.should include(@user)
    end   
    
    it "should create a new membership for unmoderated lists and automatically accepts the users membership request (create_unmoderated_memberships)" do
           
      @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
      :description => "Dear Non-Instagramers", :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))
      @list.moderated = false
      @list.save             
      @list.users.push(@user)
      @list.create_unmoderated_membership(@user)
      @list.users.should include(@user) 
      @list.user_lists.where(:user_id => @user.id).first.state.should == "accepted"     
    end
    
    it "should create a new membership for moderated lists (create_moderated_memberships)" do
                   
      @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
      :description => "Dear Non-Instagramers", :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))
      @list.moderated = true
      @list.save           
      @list.users.push(@user_2)      
      @list.create_moderated_membership(@user_2, @user_2)
      ActionMailer::Base.deliveries.last.should_not be_nil
      @list.users.size.should == 1      
    end  
    
    it "should create a membership request in the 'requested' state for a moderated membership request (create_moderated_membership)" do
                  
      @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
      :description => "Dear Non-Instagramers", :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))
      @list.moderated = true  
      @list.create_unmoderated_membership(@user)
      @list.save
      @list.create_moderated_membership(@user_2, @user_2)
      ActionMailer::Base.deliveries.last.should_not be_nil
      @list.members.size.should  == 1
      @list.request_pending_members(@user_2.id).first.user.should == @user_2
    end   
    
    it "should create a membership request (create_membership_request)" do
                    
      @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
      :description => "Dear Non-Instagramers", :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))
      @list.moderated = true
      @list.create_unmoderated_membership(@user)
      @list.send(:create_membership_request, @user_2)
      @list.request_pending_members(@user_2.id).first.user.should == @user_2
    end
    
    
    
    it "should identify if a user is the moderator for a given list" do
      @list = FactoryGirl.create(:list, :title => 'Instagram members', :creator => @user, 
      :description => "Dear Non-Instagramers", :tag => FactoryGirl.create(:tag, :keywords => 'instagram, startups entrepreneurs'))
      @list.moderated = false
      @list.save           
      @list.send(:moderator?, @user).should == true
    end
  end
  
  context "Total Votes" do
    it "get_total_upvote" do
       
      @list = FactoryGirl.build(:list, :creator => @user_2)   
      @list.save   
      @rating1 = FactoryGirl.create(:rating, :user => @user,:list=>@list)      
      @list.get_total_upvote.should == 1
    end
    
    it "get_total_downvote" do
      @list =FactoryGirl.build(:list, :creator =>@user_2)  
      @list.save       
      @rating1 = FactoryGirl.create(:rating, :user => @user,:list=>@list,:rate=>-1)      
      @list.get_total_downvote.should == 1
    end    
    
     it "get_total_vote" do
      @list = FactoryGirl.build(:list, :creator => @user_2)    
      @list.save     
      @rating = FactoryGirl.create(:rating, :user => @user,:list=>@list,:rate=>1)    
      @user1 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc',:perishable_token=> "aawindowintheskies",:single_access_token=> "1123123123")
      @user1.activate!     
      @rating1 = FactoryGirl.create(:rating, :user => @user1,:list=>@list,:rate=>-1)    
      @list.get_total_vote.should == 0         
    end
  end
  context "Get random list for landing page" do
    it "get recent list order by created" do
      @list1 = FactoryGirl.create(:list,:title=>"1", :creator => @user)    
      @list2 =  FactoryGirl.create(:list,:title=>"2", :creator => @user)    
      @list3 =  FactoryGirl.create(:list,:title=>"3", :creator => @user)    
      List.recent.should == [@list1,@list2,@list3]
    end  
    it "get most voted" do     
      @list1 = FactoryGirl.create(:list,:title=>"1", :creator => @user_2)    
      @rating = FactoryGirl.create(:rating, :rate => 1,:user=>@user,:list=>@list1)
      @list2 =  FactoryGirl.create(:list,:title=>"2", :creator => @user_2)    
      @rating = FactoryGirl.create(:rating, :rate => -1,:user=>@user,:list=>@list2)
      @list3 =  FactoryGirl.create(:list,:title=>"3", :creator => @user_2)    
      List.most_voted.should == [@list1,@list3,@list2]
    end  
  end
  context "Authorization of list" do
    it "should return true if list is not moderated" do
      @list = FactoryGirl.create(:list,:title=>"1", :creator => @user)  
      @user1 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc',:perishable_token=> "aawindowintheskies",:single_access_token=> "1123123123")
      @user1.activate!      
      @list.is_authorized?(@user1).should == true
    end
    it "should return false if list is moderated and user is not member of list" do
      @list = FactoryGirl.create(:list,:title=>"1", :creator => @user,:moderated=>true)  
      @user1 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc',:perishable_token=> "aawindowintheskies",:single_access_token=> "1123123123")
      @user1.activate!      
      @list.is_authorized?(@user1).should == false
    end
     it "should return false if list is moderated and user is  join list but not approved" do
      @list = FactoryGirl.create(:list,:title=>"1", :creator => @user,:moderated=>true)  
      @user1 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc',:perishable_token=> "aawindowintheskies",:single_access_token=> "1123123123")
      @user1.activate!      
      @list.user_lists.create(:user_id=>@user1.id)
      
      @list.is_authorized?(@user1).should == false
    end
    it "should return true if list is moderated and user is  member of list" do
      @list = FactoryGirl.create(:list,:title=>"1", :creator => @user,:moderated=>true)  
      @user1 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc',:perishable_token=> "aawindowintheskies",:single_access_token=> "1123123123")
      @user1.activate!      
      @user_list = @list.user_lists.new(:user_id=>@user1.id)
      @user_list.state = "accepted"
      @user_list.save
      @list.is_authorized?(@user1).should == true
    end
    
  end 
end  

