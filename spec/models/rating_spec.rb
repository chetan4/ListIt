require 'spec_helper'

describe Rating do
   before(:each) do
    @user = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc')
    @user.activate!    
    @user_2 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => 'abcabcabc', :single_access_token => '12221222121')
    @user_2.activate!   
    @list = FactoryGirl.create(:list, :creator => @user_2)
   end  
     
   context "Create Rating without list" do
     it "should have 1 error on list_id" do          
       @rating = FactoryGirl.build(:rating, :list_id => nil,:user=>@user)
       @rating.save
       @rating.should_not be_valid
       @rating.should have(1).error_on(:list_id)
     end
  end  
  
  context "Create Rating without user" do
    it "should have 1 error on user_id" do           
      @rating = FactoryGirl.build(:rating, :list => @list,:user_id=>nil)
      @rating.save
      @rating.should_not be_valid
      @rating.should have(1).error_on(:user_id)
    end
  end

  context "User can add one rate for one list" do
    it "should have one error on adding more than one vote on one list " do
      @rating = FactoryGirl.build(:rating, :rate => 1,:user=>@user,:list=>@list)
      @rating.save
      @rating1 = FactoryGirl.build(:rating, :rate => 1,:user=>@user,:list=>@list)
      @rating1.save
      @rating1.should_not be_valid
      @rating1.should have(1).error_on(:user_id)         
    end
  end                                              
  
  context "Create Rating with invalid value" do
    it "should have rate value  1 or -1 " do
      @rating = FactoryGirl.build(:rating, :rate => 2,:user=>@user,:list=>@list)
      @rating.save
      @rating.should_not be_valid
      @rating.should have(1).error_on(:rate)
      @rating.rate =1
      @rating.save
      @rating.rate.should == 1
    end
  end                                       
  
  context "Update Rating after 5 minutes" do
    it "should return error if vote is added after 5 minutes " do
      @rating = FactoryGirl.build(:rating, :user=>@user,:list=>@list)
      @rating.updated_at = (Time.now-10.minutes)
      @rating.save
      @rating.should_not be_valid         
    end
  end                            
  
  context "Create Rating " do
    it "should return array of rating and error and total vote count on up vote" do
      @rating= Rating.up_vote(@list.id,@user.id)
      #Create Rating
      @rating[0].should== Rating.last
      #Check error
      @rating[1].should == ""
      @rating[2].should == 1

    end
    
     it "should return array of rating and error and total vote count on down vote" do
      @rating= Rating.down_vote(@list.id,@user.id)
      #Create Rating
      @rating[0].should== Rating.last
      #Check error
      @rating[1].should == ""
      @rating[2].should == -1
     end 
  end  
  
  context "can add vote" do
    it "should return false for vote is updated after 5 minutes" do
      @rating = FactoryGirl.build(:rating, :rate => 1,:user=>@user,:list=>@list)
      @rating.updated_at = (Time.now-10.minutes)
      @rating.is_time_expires?.should == false
      @rating.can_vote?.should == false
    end
    
     it "should return true for vote is updated before 5 minutes" do

      @rating = FactoryGirl.build(:rating, :rate => 1,:user=>@user,:list=>@list)
      @rating.updated_at = (Time.now-4.minutes)  
      @rating.is_time_expires?.should == true
      @rating.can_vote?.should == true
    end 
    
    it "should return false for vote for my list" do
      @rating = FactoryGirl.build(:rating, :rate => 1,:user=>@user_2,:list=>@list)
      @rating.is_rating_for_own_list?.should == true
      @rating.can_vote?.should == false
    end
  end
  context "Undo Vote" do
   it "should set 0 if vote is upvote" do
      
      @rating = Rating.up_vote(@list.id,@user.id)
      @rating = Rating.down_vote(@list.id,@user.id)
      @rating[0].rate.should == 0 
   end
  end

end
