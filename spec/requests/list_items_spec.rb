require 'spec_helper' 

describe "ListItems" do
  before(:all) do
    activate_authlogic
  end        
  
  before(:each) do        
   # Login before each integration test
    @user = FactoryGirl.create(:user, :password => 'testtesttest', :password_confirmation => 'testtesttest') 
    @user.activate!              
    puts "USER = #{@user.inspect}"      
    @list = FactoryGirl.create(:list, :creator => @user)    
    visit new_user_session_path
    fill_in "user_session_email", :with => @user.email
    fill_in "user_session_password", :with => 'testtesttest'
    click_button "Login"         
    page.should have_content "My Lists"  
  end
  
  describe "Url/Bookmark creation" do
    it "should create a bookmark and redirect the user to the url#index page" do      
      visit new_list_url_url(@list)        
      page.should have_content('New Url')
      fill_in "url[url]", :with => 'http://techcrunch.com/2012/04/15/an-open-letter-to-those-not-employed-at-instagram/'
      fill_in "url[title]", :with => "An Open Letter To Those Not Employed At Instagram"
      click_button "Save Changes"      
      page.should have_content "An Open Letter To Those Not Employed At Instagram"
    end 
    
    it "Should not create a bookmark if the title or the url is not present" do
      visit new_list_url_url(@list)        
      page.should have_content('New Url')
      fill_in "url[url]", :with => 'http://techcrunch.com/2012/04/15/an-open-letter-to-those-not-employed-at-instagram/'
      click_button "Save Changes"
      page.should have_content "can't be blank"      
      page.should have_content "New Url"      
    end
    
    it "should not allow non members to add urls to a list" do
      click_link "Logout"
      @user_2 = FactoryGirl.create(:user, :password => "abcabcabc", :password_confirmation => "abcabcabc", :single_access_token => '122212221222')
      @user_2.activate!
      @list_2 = FactoryGirl.create(:list, :creator => @user_2)
      visit new_user_session_path
      fill_in "user_session_email", :with => @user_2.email
      fill_in "user_session_password", :with => 'abcabcabc'
      click_button "Login"         
      page.should have_content "My Lists"  
      visit list_urls_url(@list)
      page.should_not have_content "New Url"      
    end
  end     
end
