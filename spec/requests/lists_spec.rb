require 'spec_helper'  
require 'authlogic/test_case'

describe "Lists" do    
  before(:all) do
    activate_authlogic
  end
  
  before(:each) do        
    # Login before each integration test
    @user = FactoryGirl.create(:user, :password => 'testtesttest', :password_confirmation => 'testtesttest') 
    @user.activate!    
    visit new_user_session_path
    fill_in "user_session_email", :with => @user.email
    fill_in "user_session_password", :with => 'testtesttest'
    click_button "Login"         
    page.should have_content "My Lists"                                    
  end

  it "loads the list creation pages on clicking the new list button" do 
    visit lists_path
    click_link "Create A New List"
    page.should have_content('New List')
  end 

  describe "New list creation" do
    it "Creates a new list and loads the lists page" do
      visit new_list_path
      fill_in "list_title", :with => "Sunspot solr"
      fill_in "list_description", :with => "Contains researched material about Sunspot Solr."
      click_button "Save Changes"
      page.should have_content "Sunspot solr"       
    end                       
  end
    
  describe "New list creation failure" do
    it "Should not create a new list if the list description is missing" do
      visit new_list_path                                                  
      page.should have_content "New List"
      fill_in "list_title", :with => "Sunspot solr"
      click_button "Save Changes"
      page.should have_content "New List"
    end
  end
end

