require 'spec_helper.rb'

class DummyClass
end

describe "ContentGrabber" do
  before(:each) do
    DummyClass.send(:include, ActiveRecord::ContentGrabber)
    @valid_url = "http://highscalability.com/blog/2012/4/9/the-instagram-architecture-facebook-bought-for-a-cool-billio.html"
    @invalid_url = "http://calability.com/blog/2012/4/9/the-instagram-architecture-facebook-bought-for-a-cool-billio.html"
  end 

  context "get_url_contents" do
    it "should fetch the url contents" do         
      doc = DummyClass.new.get_url_contents(@valid_url)
      doc.class.should == Nokogiri::HTML::Document      
    end     
  end
  
  context "grab_url_contents" do
    it "should not throw an error if the url is invalid" do
      lambda {DummyClass.new.get_url_contents(@invalid_url)}.should_not raise_error()
      lambda {DummyClass.new.get_url_contents(@invalid_url)}.call.should == nil      
    end    
  end
                                            
  context "get_site_title" do
    it "should return an array of titles for the website" do
      dummy_class_instance = DummyClass.new
      dummy_class_instance.get_url_contents(@valid_url)
      dummy_class_instance.get_site_title.should be_an_instance_of(Array)    
      dummy_class_instance.get_site_title.first.should be_an_instance_of(String) 
    end   
  end
  
  context "get_page_description" do
    it "should return an array of meta tags or description blocks" do
      dummy_class_instance = DummyClass.new
      dummy_class_instance.get_url_contents(@valid_url)
      dummy_class_instance.get_site_description.should be_an_instance_of(Array)
      dummy_class_instance.get_site_description.first.to_s.should be_instance_of(String)
      dummy_class_instance.get_site_description.first.to_s.strip.gsub('\n','').should == "It's been a well kept secret, but you may have heard  Facebook will Buy Photo-Sharing Service I..."
    end
  end       
    
  context "get_site_images" do
    it "should fetch images from the page" do          
      dummy_class_instance = DummyClass.new
      dummy_class_instance.get_url_contents(@valid_url)
      dummy_class_instance.get_site_images.should be_instance_of(Array)
      dummy_class_instance.get_site_images.first.to_s.should be_instance_of(String)
      dummy_class_instance.get_site_images.first.to_s.should == 'http://farm8.staticflickr.com/7011/6464246201_bddb8c499e_o.jpg'
    end
  end
  
  context "grab_site_content" do
    it "should fetch images, description, title for a given page" do
      response = DummyClass.grab_url_contents(@valid_url)
      response.should be_an_instance_of(Hash)
      response.keys.should include(:titles, :keywords, :images, :description)
    end
  end


end