class AddStateToUserLists < ActiveRecord::Migration
  def change
    add_column :user_lists, :state, :string
  end
end
