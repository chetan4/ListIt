class AddTotalVoteToLists < ActiveRecord::Migration
  def change
    add_column :lists, :total_vote, :integer,:default=>0
  end
end
