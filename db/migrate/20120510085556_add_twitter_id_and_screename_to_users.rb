class AddTwitterIdAndScreenameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :twitter_id, :string
    add_column :users, :twitter_screenname, :string
    add_column :users, :twitter_avatar, :string
  end
end
