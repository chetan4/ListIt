class CreateListItems < ActiveRecord::Migration
  def change
    create_table :list_items do |t|
      t.string :title
      t.string :url
      t.text :description
      t.integer :list_id
      t.integer :owner_id
      t.string :type
      t.timestamps
    end
  end
end
