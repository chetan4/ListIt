class ChangeCryptedPasswordLimit < ActiveRecord::Migration
  def up
    change_column :users, :crypted_password, :string, :limit => nil
  end

  def down
    change_column :users, :crypted_password, :string
  end
end
