class AddIndexToListItems < ActiveRecord::Migration
  def change 
    add_index :list_items, :user_id
  end
end
