class AddIndexToUserList < ActiveRecord::Migration
  def change 
    add_index :user_lists, [:user_id, :list_id]
  end
end
