class AddModeratedToLists < ActiveRecord::Migration
  def change
    add_column :lists, :moderated, :boolean, :default => false
  end
end
