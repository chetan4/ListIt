class User < ActiveRecord::Base
  acts_as_authentic do |c|
  end # block optional   

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => '/assets/default_user.png'
                 
  has_many :user_lists,  :dependent => :destroy
  has_many :created_lists, :class_name => 'List', :foreign_key => 'user_id'
  has_many :lists, :through => :user_lists
  has_many :list_items
  has_many :subscriptions
  has_many :ratings 
  has_many :comments 
  has_many :projects, dependent: :destroy

  attr_accessible :name, :email, :password, :password_confirmation, :active, :description, :avatar, :twitter_id, :twitter_screenname, :twitter_avatar
  before_avatar_post_process :process_only_images

  validates_uniqueness_of :twitter_id, :if => Proc.new{|user| user.twitter_id.present?}

  validates_attachment_content_type :avatar, 
                        :message => 'Please upload correct format image',
                        :content_type => %w( image/jpeg image/png image/gif image/pjpeg image/x-png ),
                        :unless => Proc.new{|user|user.avatar.url.include?("missing.png")}
  paginates_per 25

  def active?
    active
  end

  def subscribed?(list)
    subscription = Subscription.where(:user_id => self.id, :list_id => list.id).first
    !subscription.nil?
  end

  def add_subscription_for(list)
    subscription = subscriptions.new(:list_id => list.id, :user_id => self.id)
    subscription.save
  end

  def remove_subscription_for(list)
    subscription = subscriptions.where(:list_id => list.id, :user_id => self.id).first
    subscription.destroy
  end

  def send_forgot_password!
    deactivate!
    reset_perishable_token!
    Notifier.forgot_password(self).deliver
  end

  def activate!
    self.active = true
    save
  end

  def deactivate!
    self.active = false
    save
  end

  def send_activation_instructions!
    reset_perishable_token!
    Notifier.activation_instructions(self).deliver
  end

  def send_activation_confirmation!
    reset_perishable_token!
    Notifier.activation_confirmation(self).deliver
  end

  def email_address_with_name
    "#{self.name} <#{self.email}>"
  end

  def is_admin?
    admin
  end

  # link existing users account with twitter
  def link_account(auth_hash)
    if auth_hash.present?
      parameters = {:user => {}}
      parameters[:user][:twitter_id] = auth_hash.uid
      parameters[:user][:twitter_screenname] = auth_hash.info.nickname
      parameters[:user][:twitter_avatar] = auth_hash.info.image if auth_hash.info.image.present?
      status = self.update_attributes(parameters[:user])
    else
      status = false
    end

    status
  end

  # creates a non-existing user using Twitter information
  def self.create_new_twitter_user(auth_hash)
    user = User.new(name: auth_hash.info.name, 
      twitter_id: auth_hash.uid, 
      twitter_screenname: auth_hash.info.nickname)
    user.twitter_avatar = auth_hash.info.image if auth_hash.info.image.present?
    user.generate_password
    user.generate_single_access_token
    if user.valid_twitter_user?
      status = user.save(:validate => false)
    else
      status = false
    end
    [status, user]
  end

  def valid_twitter_user?
    status = true
    user = User.where(:twitter_id => self.twitter_id).first
    if user.present?
      status = false
      user.errors.add(:twitter_id, "Must be unique")
    end

    status  
  end

  def generate_password    
    self.password = self.password_confirmation = random_string(8)
  end 


  def generate_single_access_token
    self.single_access_token = random_string(16)
  end

  private  

  def random_string(size)
    random_string = SecureRandom.hex(size)
  end
  
  def process_only_images
    if !(avatar.content_type =~ %r{^(image|(x-)?application)/(x-png|pjpeg|jpeg|jpg|png|gif)$})
      return false
    end
  end
end
