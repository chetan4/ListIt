class Tag < ActiveRecord::Base
  attr_accessible :keywords, :taggable_id, :taggable_type
   belongs_to :taggable, :polymorphic => true
end
