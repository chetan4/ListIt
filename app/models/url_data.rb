class UrlData
  include ActiveRecord::ContentGrabber
  attr_accessor :fetch_url,:title,:description,:images,:keywords,:errors
  def initialize(fetch_url)
    @fetch_url = fetch_url
   if @fetch_url.match(/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix)
    
    set_data
   else
   @errors = "Url is not valid"
   end 
  end
  def set_data
    meta_data = UrlData.grab_url_contents(@fetch_url)
    @title= meta_data[:titles]
    @description=   meta_data[:description]
    @images = meta_data[:images]
    @keywords = meta_data[:keywords]
    @errors = ''
  end  
end
