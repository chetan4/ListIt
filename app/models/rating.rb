class Rating < ActiveRecord::Base
  attr_accessible :list_id, :rate, :user_id
  validates_presence_of :user_id,:rate,:list_id
  validates_inclusion_of :rate,:in => [1,0,-1]
  validates_uniqueness_of :user_id, :scope => :list_id,:message=>"Can't vote for multiple times"
  validate :can_add_vote#,:comment_exist
  
  belongs_to :user
  belongs_to :list
  after_save :update_list_count

  def update_list_count
     List.find(self.list_id).update_attributes(:total_vote=>Rating.where("list_id"=>self.list_id).sum("rate"))
  end
 
  
  class << self
    def up_vote(list_id,user_id)
      rating = find_or_create_rating(list_id,user_id)
      error = ""
      
      if rating.is_a?(Rating)
        if rating.can_vote?
          rating.rate = rating.rate == -1 ?  0 : 1
          rating.save
          
          
        else
           error << "You can't vote time expires" unless rating.is_time_expires?
          error << "You can't vote for your own list" if rating.is_rating_for_own_list?
        end
      else
        error << rating
      end
      list = List.find(rating.list.id)
      [rating,error,list.total_vote]
    end
    
    def down_vote(list_id,user_id)
      rating = find_or_create_rating(list_id,user_id)
      error = ""
      if rating.is_a?(Rating)
        if rating.can_vote?
          rating.rate = rating.rate == 1 ? 0 : -1
          rating.save
        else
          error << "You can't vote time expires" unless rating.is_time_expires?
          error << "You can't vote for your own list" if rating.is_rating_for_own_list?
        end
      else
        error << rating
      end
      list = List.find(rating.list.id)
      [rating,error,list.total_vote]
    end
      
    def find_or_create_rating(list_id,user_id)
      list = List.find(list_id)
      user = User.find(user_id)    
      rating = ""
      if list.present? && user.present?
        rating = list.ratings.find_or_create_by_user_id(:user_id=>user_id)
      elsif user.present?
        rating=  "List is not present"
      else
        rating=  "User is not present"  
      end
    end
  end
  def can_add_vote
    self.errors.add :base ,"You can't vote time expires" unless is_time_expires?
    self.errors.add :base ,"You can't vote for your own list" if is_rating_for_own_list?
  end
  def comment_exist
    #self.errors.add :base ,"You can't down vote comment required" unless comment    
  end
  def can_vote?
    !is_rating_for_own_list? && is_time_expires?
  end
  
  def is_time_expires?
    (self.updated_at.nil? || self.updated_at > (Time.now-5.minutes) )
  end
  
  def is_rating_for_own_list?
    self.list.present? ? self.user == self.list.creator : true
  end
  
 
end
