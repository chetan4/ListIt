class SearchDelegate
  def self.list_search(search_query, page) 
    response = Sunspot.search(List) do
      fulltext search_query
      paginate :page => page, :per_page => 10 
    end                                                  

    [response.results, response]
  end       
  
  def self.url_search(search_query, page, list_id)
    response = Sunspot.search(ListItem) do
      fulltext search_query  
      with :list_id, list_id.to_i         
      paginate :page => page, :per_page => 10
    end
    
    [response.results, response]
  end
end