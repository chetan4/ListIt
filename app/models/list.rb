class List < ActiveRecord::Base
  attr_accessible :description, :owner_id, :title, :tag_attributes,:total_vote
  validates_presence_of :title, :description
  # belongs_to :owner,:class_name => 'User',:foreign_key => "owner_id"
  has_many :user_lists, :dependent => :destroy
  has_many :users, :through => :user_lists
  has_many :list_items, :dependent=>:destroy
  has_many :urls,:dependent => :destroy
  has_many :ratings
  has_one :tag, :as => :taggable             
  has_many :subscriptions
  has_many :subscribers, :through => :subscriptions, :source => :user
  belongs_to :creator, :class_name => 'User', :foreign_key => 'user_id'
  scope :recent, order("created_at desc")
  scope :most_voted, order("total_vote desc")      
  accepts_nested_attributes_for :tag
  paginates_per 10  
    
  searchable do
    text :title
    text :description
    text :list_tags
    text :urls do
       urls.each {|url| url.title }
    end                  
    text :url_description 
    text :url_keywords
    integer :list_item_count    
    time :created_at
    time :updated_at    
  end                    
  
  def latest_updated_time
    timestamps = [updated_at]
    list_items.each do|list_item|
      timestamps << list_item.updated_at
    end
    timestamps << tag.updated_at if tag
    timestamps.sort.reverse.first
  end

  def url_description
    self.urls.collect(&:description).join(' ').gsub(/,/, '').gsub(/\n\r/, ' ')
  end                              
  
  def url_keywords
   tags = self.urls.select {|url| url.tag if url.tag.present? }
   tags.collect{ |_tag| _tag.keywords}.join(' ').gsub(/,/, '').gsub(/\n\r/, ' ')
  end
  
  def list_tags
    self.tag.keywords.gsub(/,/, '').gsub(/\n\r/, ' ')    if self.tag
  end                       
  
  def list_item_count
    self.urls.count
  end   
  
  def get_total_upvote
    self.ratings.where(:rate=>1).count
  end

  def get_total_downvote
    self.ratings.where(:rate=>-1).count
  end

  def get_total_vote
    self.total_vote
  end
                                                 
  # Adds a user to a list as a member
  # Verifies if the person requesting this membership is the moderator. If not, sends an email to the creator of the list with the details of the user.  
  # request: User ( The person initiating the request)
  # user: User ( The person to be added to the list)
  def create_moderated_membership(user, requester)  
    status = false
    if moderator?(requester)
      create_membership(user) 
      status = true
    else                                                           
      create_membership_request(user)
      Notifier.membership_request(self.creator, user, self).deliver      
      status = false                                               
    end  
    
    status
  end
  
  def create_unmoderated_membership(user)
    create_membership(user)
    status = true
  end       
  
  def reject_membership(user)
    if self.users.include?(user)
      transaction do
        user_list = self.user_lists.where(:user_id => user.id, :state => 'requested').first
        user_list.reject!
        user_list.destroy
      end
    end
  end 
  
  def members
    self.user_lists.where(:state => "accepted").includes(:user).map(&:user)
  end 
  
  def request_pending_members(user_id)
    self.user_lists.where(:state => "requested", :user_id => user_id)
  end

  def self.get_random_lists_for_landing_page
    #get most recent list   
    if rand(2) == 0
      List.recent.limit(15)
    #get most vote list
    else
      List.most_voted.limit(15)
    end
  end
          
  def is_authorized?(user)
    moderated && (moderator?(user) || members.include?(user)) || !moderated
  end    
                       
  private            
  
  def create_membership_request(user)
    self.users.push(user) if !self.users.include?(user)
  end
                                
  def create_membership(user)
    if !self.users.include?(user) 
      transaction do
        self.users.push(user) 
        user_list = self.user_lists.where(:user_id => user.id).first
        user_list.accept!
      end                                      
    else
      user_list = self.user_lists.where(:user_id => user.id, :state => 'requested').first
      user_list.accept! if user_list.present?
    end
  end  
  
  def moderator?(user)
    self.creator == user
  end
end

