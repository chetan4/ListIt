class UserList < ActiveRecord::Base
  attr_accessible :list_id, :user_id  
  belongs_to :user
  belongs_to :list
  
  state_machine :state, :initial => :requested do
    event :accept do
      transition [:requested] => :accepted
    end    
    
    event :reject do
      transition [:requested, :accepted] => :rejected
    end
    
    event :request do
      transition [:rejected] => :requested
    end
  end
                     
  # Fetches all the List membership requests for a given user
  def self.requested(user)       
    list_ids = user.lists.where(:user_id => user.id).collect(&:id)
    #list_ids = user.lists.collect(&:id)
    UserList.where("state = 'requested' AND list_id IN (?)", list_ids).includes(:list => :users)
  end    
end
