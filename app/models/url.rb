class Url < ListItem  
  attr_protected :user_id         
  validates_format_of :url, :with => /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix
  paginates_per 10                
  has_many :comments, :as => :commentable
  def keywords
    self.tag.keywords.gsub(/,/, '').gsub(/\n\r/, '').to_s  if self.tag.present? && self.tag.keywords.present?
  end       
  
end
