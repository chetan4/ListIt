class ListItem < ActiveRecord::Base
  attr_accessible :description, :list_id, :owner_id, :title, :url,:tag_attributes
  attr_protected :user_id
  belongs_to :creator, :class_name => 'User', :foreign_key => 'user_id'
  belongs_to :list
  validates_presence_of :title
  has_one :tag, :as => :taggable
  accepts_nested_attributes_for :tag

  scope :of_type, lambda{|class_type| {:conditions => { :type => class_type}}}
  
  searchable do
    text :title
    text :url
    text :description
    integer :list_id, :references => List
    text :keywords
    string :list_item_type
  end

  def list_item_type
    self.type
  end    
end
