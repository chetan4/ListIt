class Notifier < ActionMailer::Base
  def activation_instructions(user) 
    @account_activation_url = activate_account_url(user.perishable_token)

    mail(:to => user.email_address_with_name,
    :subject => "Activation Instructions",
    :from => "Fire List Notifier <noreply@firelist.it>",
    :fail_to => "Fire List Notifier <noreply@firelist.it>"
    ) do |format|
      format.text
    end
  end

  def activation_confirmation(user)

    mail(:to => user.email_address_with_name,
    :subject => "Activation Complete",
    :from => "Fire List Notifier <noreply@firelist.it>",
    :fail_to => "Fire List Notifier <noreply@firelist.it>"
    ) do |format|
      format.text
    end
  end

  def forgot_password(user)
    @reset_password_link = reset_password_url(user.perishable_token)
    mail(:to => user.email_address_with_name,
    :subject => "Password Reset",
    :from => "Fire List Notifier <noreply@firelist.it>",
    :fail_to => "Fire List Notifier <noreply@firelist.it>"
    ) do |format|
      format.text
    end
  end

  def membership_request(moderator, new_membership_requester, list)
    @list = list
    @moderator = moderator
    @new_membership_requester = new_membership_requester
    mail(:to => moderator.email_address_with_name,
    :subject => "Request to join List ",
    :from => "Fire List Notifier <noreply@firelist.it>",
    :fail_to => "Fire List Notifier <noreply@firelist.it>"
    ) do |format|
      format.html
    end
  end

  def for_subscription(list, subscribers, list_urls)
    @list = list
    @list_urls = list_urls
    email_addresses_with_names = subscribers.collect(&:email_address_with_name).join(', ');

    mail(:to => email_addresses_with_names,
         :subject => "New urls on list #{@list.title}",
         :from => "Fire List Notifier <noreply@firelist.it>",
         :fail_to => "Fire List Notifier <noreply@firelist.it>"
        ) do |format|
          format.html
        end
  end
end
