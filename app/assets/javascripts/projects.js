// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var LI = LI || {};

LI = (function($, window, document, ListIt){
	ListIt.Projects = {
		new: function(modalSelector){			
			var self = this;
			$('.container-fluid').delegate('.create-new-project','click', function(evt){				
				evt.preventDefault();
				$(modalSelector).modal({
					backdrop:true,
					keyboard:true, 
					show:true
				});


			});
		},

		activateCreate: function(modalSelector){				
			$('.container-fluid').delegate('.new-project-form', 'submit', function(evt){				
				evt.preventDefault();
				var $form = $(this);

				$form.find('.submit-btn').first().disableButton('Loading...');
				var ajaxOptions = {
					url: $(this).attr('action'),
					type: 'post',
					data: $form.serialize()
				};

				ListIt.Ajax.sendRequest(ajaxOptions, function(response){	
					if(response.status == 'success'){
						$('.container-fluid').find('.each-project').last().after(response.data);
						$(modalSelector).modal('hide');	
						$form.clearForm();
					}	
					else{
						var errorMessage = '<div class="alert alert-error error-message">' + response.errors + '</div>';
						$form.before(errorMessage)	
						window.setTimeout(function(){
							$form.parent().find('.error-message').remove();
						}, 5000);
					}					

					$form.find('.submit-btn').first().enableButton('Save changes');
				});
			});
		}
	}

	// load document
	$(document).ready(function(){		
		if($('.container-fluid').find('.create-new-project').length > 0){			
			ListIt.Projects.new('#project-form-modal');
			ListIt.Projects.activateCreate('#project-form-modal');
		}
	});

	return LI;
})(jQuery, this, this.document, LI);