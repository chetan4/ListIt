// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var LI = LI || {};

LI = (function($, window, document, ListIt){
 	 ListIt.listRating = {
 
 	 upVote :function(url,self){  
 	  var parent_list = ".list_"+$(self).data("list_id");
 	 
	 	  var ajaxOptions = {
				url: url
	     };
	     ListIt.Ajax.sendRequest(ajaxOptions, function(json_data){ 
        $(parent_list).find("#vote_value").first().text(json_data.vote);
   
        if(json_data.error)
        { 
        var error_selector = parent_list+" .vote_error"

         var error_selector = $(parent_list+" .vote_error");
         error_selector.attr('title',json_data.error)
         error_selector.popover({placement:'left'});
         error_selector.popover('show');
         setTimeout(function(){error_selector.popover('hide')},5000);
          

        }
 		    if(json_data.location){
          window.location.href = json_data.location;
        }
			 });		
	 	 },
	 	  downVote :function(url,self){
	 	   var parent_list = ".list_"+$(self).data("list_id");
  	 	 var ajaxOptions = {
				url: url
		   };
	     ListIt.Ajax.sendRequest(ajaxOptions, function(json_data){ 	     
        $(parent_list).find('#vote_value').first().text(json_data.vote);
        if(json_data.error)
        {
          var error_selector = $(parent_list+" .vote_error");
          error_selector.attr('title',json_data.error)
          error_selector.popover({placement:'left'});
          error_selector.popover('show');
          setTimeout(function(){error_selector.popover('hide')},5000);
         } 
 		    if(json_data.location){
          window.location.href = json_data.location;
         }
			 });		
	 	 }
	 	};
	 	$(document).ready(function(){
			 $('.up_vote').on("click",function(e){
			  e.preventDefault();
				ListIt.listRating.upVote($(this).attr('href'),this);
			});
			$('.down_vote').on("click",function(e){
			 e.preventDefault();
			ListIt.listRating.downVote($(this).attr('href'),this);
			});
		});
		return LI;
})(jQuery, this, this.document, LI);

