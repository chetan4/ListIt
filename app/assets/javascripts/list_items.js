// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
var LI = LI || {};

LI = (function($, window, document, ListIt){
	 	ListIt.autoLoadUrlContent = {
		  
		  isValidUrl: function(url){
			  	var RegExp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

			    if(RegExp.test(url))
			        return true;
			    else
			        return false;			    
			},
			
			// Fetches site contents once URL is entered into Text Field.		
		  fetchContent: function(url){
				var self = this;   				
				
				$('.new_url').delegate('#url_url', 'blur', function(e){   					
				 	e.preventDefault();                                     
					var listUrl = $(this).val();					
					if((listUrl != "") && (self.isValidUrl(listUrl)))
					{
 						$('.loader').removeClass('hidden');
						
						var ajaxOptions = {
							url: '/fetch_url_content?url='+ listUrl,
							error: function(errorResponse){
								if($('.loader').length > 0)
									$('.loader').addClass('hidden');
							} 
						};            

						// Ajax call.
						ListIt.Ajax.sendRequest(ajaxOptions, function(json_data){
							$('.loader').addClass('hidden'); 
							$("#url_title").val(''); 
							$("#url_description").val('');
							$("#url_error").text('');
							$("#url_tag_attributes_keywords").val('');
							$("#url_title").val(json_data.title);
							$("#url_tag_attributes_keywords").val(json_data.keywords);
							$("#url_description").val(json_data.description);
							$("#url_image_url").val(json_data.images[0]);
							$("#url_error").text(json_data.errors);		      
						});				  						
					}
				});
			}
		};
		
		$(document).ready(function(){
			 if($('.new_url').length > 0){ 
			 		ListIt.autoLoadUrlContent.fetchContent();
			}
		});  
		
		return LI;
	
})(jQuery, this, this.document, LI);


