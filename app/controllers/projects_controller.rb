class ProjectsController < ApplicationController
  layout 'projects'
  before_filter :require_user

  
  def index
    @projects = current_user.projects.order('created_at DESC')
  end

  def create
    @projects = current_user.projects.order('created_at DESC')
    @project = current_user.projects.new(params[:project])
    respond_to do |format|
      if @project.save
        format.js{
          render :json => {status: 'success', project: @project, 
            data: render_to_string(partial: 'each_project', locals: {project: @project}) }
        }

        format.html{}
      else
        format.js {
          render :json => {status: 'error', errors: @project.errors.full_messages.join(', ')}
        }        
        format.html{}
      end
    end
   end

  def show
  end

  def new
  end

  def edit
  end

  def update
  end

  def destroy
    @project = current_user.projects.find(params[:id])
    respond_to do |format|
      if @project.destroy
        format.js {
          render :json => {status: 'success'}          
        }

        format.html{
          flash[:notice] = "Your project has been successfully removed."
          redirect_to projects_url
        }
      else
        format.js{
          render :json => {status: 'error', errors: 'The project could not be deleted at this moment'}
        }

        format.html{
          flash.now[:error] = "Your project could not be deleted at this moment."
          render "index"
        }
      end
    end
  end
end
