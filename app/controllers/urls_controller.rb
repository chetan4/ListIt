class UrlsController < ApplicationController
  # GET /list_items
  # GET /list_items.json
  before_filter :set_permission_for_landing_page,:only=>[:index]
  before_filter :require_user
  before_filter :load_list, :except=>[:fetch_url_content] 
  before_filter :authorized_to_create?, :only => [:new, :create]

  def search                                                   
    @page = params[:page] || 1
    @urls, @paginatable_urls = SearchDelegate.url_search(params[:search_query],  @page, @list.id)
    render :action => "index"
  end

  def index                          
    page = params[:page] || 1
    @urls = @list.urls.page(page)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @urls }
    end
  end

  # GET /list_items/1
  # GET /list_items/1.json
  def show
    @url = @list.urls.find(params[:id])
    @comments = @url.comments.includes(:user)
    @comment = @url.comments.new
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @url }
    end
  end

  # GET /list_items/new
  # GET /list_items/new.json
  def new
    @url = @list.urls.new
    @tag = @url.tag || @url.build_tag
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @url }
    end
  end

  # GET /list_items/1/edit
  def edit
    @url = @list.urls.find(params[:id])
    @tag = @url.tag || @url.build_tag
  end

  # POST /list_items
  # POST /list_items.json
  def create
    @url = @list.urls.new(params[:url]) 
    @url.creator = current_user
    
    respond_to do |format|
      if @url.save        
        @list.solr_index!
        format.html { redirect_to list_urls_url(@list), notice: 'List item was successfully created.' }
        format.json { render json: @url, status: :created, location: @url }
      else
        @tag = @url.tag || @url.build_tag
        format.html { render action: "new" }
        format.json { render json: @url.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /list_items/1
  # PUT /list_items/1.json
  def update                      
    @url = @list.urls.find(params[:id])
    @tag = @url.tag || @url.build_tag
    respond_to do |format|
      if @url.update_attributes(params[:url])        
        @list.solr_index!
        format.html { redirect_to list_urls_url(@list), notice: 'List item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @url.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /list_items/1
  # DELETE /list_items/1.json
  def destroy
    @url = @list.urls.find(params[:id])
    respond_to do |format|
      if @url.destroy
        @list.solr_index!
        format.html { redirect_to list_urls_url }
        format.json { head :no_content }
      else
        format.html { redirect_to :back }
        format.json {render json: @url.errors, status: :unprocessable_entity}
      end
    end
  end

  def fetch_url_content
    @url = UrlData.new(params[:url])
    render json:@url.to_json
  end
  def add_comment
  
    @url = @list.urls.find(params[:id])
    @comment = @url.comments.new(:body=>params["body"],:user_id=>current_user.id)
    if @comment.save  
      flash[:notice] = "Comment successfully created"
    else 
      flash[:notice] = "Comment is not created"
    end
    redirect_to list_url_path(@list,@url)
    
  end
  protected
  def load_list
    @list = List.find(params[:list_id])
  end
  
  private
  
  def authorized_to_create?
    unless @list.is_authorized?(current_user)
      flash[:notice] = "You cannot add to this list. This is a moderated list and requires approval by the creator of the list"
      redirect_to list_urls_url(@list)
    end
  end
end
