class MembersController < ApplicationController
  before_filter :set_permission_for_landing_page,:only=>[:index]
  before_filter :require_user          
  before_filter :require_list, :except => [:manage]
  
  # GET /lists
  # GET /lists.json
  def index
  	@list = List.find(params[:list_id])
  	@members = @list.users.includes(:lists).order('created_at DESC')
	end     

  def show
    @member = (current_user.id.to_s == params['id']) ? current_user : User.find(params['id'])
    @list = List.includes(:users).find(params[:list_id])
  end
	
	def create                                  
	  @user = User.find(params[:id])	  
    if @list.moderated?
      status = @list.create_moderated_membership(@user, current_user)
    else
     status = @list.create_unmoderated_membership(@user)
    end
              
    if status == true
      flash[:notice] = "You have been successfully added to this list."  
      flash[:notice] = "#{@user.name} has been successfully added to this list." if current_user != @user
    else
      flash[:info] = "Your request to join this list has been sent to the moderator."
    end
    
    respond_to do |format|
      format.html { redirect_to list_urls_url(@list)}
    end
  end 
     
  # allows only the current user to be removed from a group
  def destroy
    @user = current_user
    if @list.users.include?(current_user) && @list.creator != current_user
      if @list.users.destroy(@user)
        flash[:notice] = "Your are no longer part of this list."
      end
    else                                                  
      flash[:error] = "You are not part of this group."
      flash[:error] = "You are the creator of this list. To leave this group please delete this list."  if @list.creator == current_user
    end                                                                                                                                 
    redirect_to list_urls_url(@list)    
  end 
  
  def manage
    @user = current_user
    @requests = UserList.requested(current_user)
  end  
  
  def reject
    @user = User.find(params[:id])
    if current_user == @list.creator       
      @list.reject_membership(@user)
    else
      flash[:notice] = "Only the moderator of this room can grant access permissions."
    end                            
    
    redirect_to manage_members_url
  end
     
  private
  def require_list
    @list = List.find(params[:list_id])
  end

end
