

class Admin::AdminController < ApplicationController
 before_filter :require_user 
 before_filter :set_user
 
 
 private
 def set_user
  @admin_user = current_user
  unless @admin_user.is_admin?
    flash[:notice] = "You must be admin to access this page"
    redirect_back_or_default(lists_url)
  end
    
 end
end
