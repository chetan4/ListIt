class Admin::ListsController < Admin::AdminController
  def index
    @user = User.find(params[:user_id])
    @lists = @user.created_lists.page(params[:page])
  end
  def destroy
    @user = User.find(params[:user_id])
    @list = List.find(params[:id])
    flash[:notice]= "List is successfully deleted"
    @list.destroy
    redirect_to admin_user_list_path(@user)
    
  end
end
