class Admin::UsersController < Admin::AdminController
  def index
   @users = User.order(:name).page(params[:page],:per_page => 25 )
  end
  def show
   @user = User.find(params[:id])
  end
  
   def destroy
    @user = User.find(params[:id])

    if @admin_user != @user
      @user.destroy
      flash[:notice] ="User is successfully deactivated"
    else
      flash[:notice] ="User can't deactivated"  
    end
    
    redirect_to admin_users_path
  end
  def deactivate
    @user = User.find(params[:id])

    if @admin_user != @user
      @user.deactivate!
      flash[:notice] ="User is successfully deactivated"
    else
      flash[:notice] ="User can't deactivated"  
    end
    
    redirect_to admin_users_path
  end
  def activate
    @user = User.find(params[:id])
    @user.activate!
    flash[:notice] ="User is successfully activated"
    redirect_to admin_users_path
  end
  def destroy
    
  end
end
