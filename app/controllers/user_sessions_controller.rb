class UserSessionsController < ApplicationController
  layout "welcome"
  before_filter :require_no_user, :only => [:new, :create]
  before_filter :require_user, :only => :destroy

  def new
    @user_session = UserSession.new
    @lists = List.get_random_lists_for_landing_page
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      flash[:notice] = "Login successful!"
      redirect_back_or_default lists_url
    else                     
      @lists = List.get_random_lists_for_landing_page
      flash[:error] = "Username or Password is invalid."
      render :action => :new
    end
  end

  def destroy
    current_user_session.destroy
    flash[:notice] = "Logout successful!"
    redirect_back_or_default new_user_session_url
  end

  def forgot_password
    if current_user
      redirect_to edit_account_url
    else
      @user_session = UserSession.new()
    end
  end

  def forgot_password_lookup_email
    if current_user
      redirect_to edit_account_url
    else
      user = User.find_by_email(params[:user_session][:email])
      if user
        user.send_forgot_password!
        flash[:notice] = "A link to reset your password has been mailed to you."
      else
        flash[:notice] = "Email #{params[:user_session][:email]} wasn't found.  Perhaps you used a different one?  Or never registered or something?"
        render :action => :forgot_password
      end
    end
  end    

  def twitter_auth
    @user = current_user
    auth_hash =  request.env['omniauth.auth']
    if @user.present?
      status = @user.link_account(auth_hash)
      status ? flash[:notice] = "Your account has been successfully linked." : flash[:error] = "Your account could not be linked at this moment."
      redirect_to user_url(@user)
    else
      create_or_authenticate(auth_hash)     
    end    
  end

  def create_or_authenticate(auth_hash)
    if @user = User.where(twitter_id: auth_hash.uid, active:true).first
      login_user 
    else
      status, @user = User.create_new_twitter_user(auth_hash)
      if status && @user
        @user.activate!
        login_user     
      else
        @user_session = UserSession.new
        @user_session.errors.add(:base, "Invalid") 
        @lists = List.get_random_lists_for_landing_page       
        flash.now[:error] = "Your account could not be created. Please try registering using a username and password."
        render "new"
      end
    end
  end

  private

  def login_user
    UserSession.create!(@user, true)
    redirect_to lists_url
  end
end
