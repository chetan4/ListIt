class ListsController < ApplicationController
  before_filter :set_permission_for_landing_page,:only=>[:up_vote,:down_vote]
  before_filter :set_user
  before_filter :require_user 
  
  def search   
    @page = params[:page] || 1
    @lists, @paginatable_lists = SearchDelegate.list_search(params[:search_query], @page)
    render :action => "index"
  end 

  # look at a members lists
  def peek              
    @user = User.find(params[:user_id])
    @lists = @user.lists.includes(:list_items, :users).order('created_at DESC').page(params[:page])
    @title = "#{@user.name}'s Lists"
    respond_to do |format|  
      format.html { render :action => "index"}
      format.json { render json: @lists }
    end    
  end                   
  
  def all
    @lists = List.includes(:list_items, :users, :tag).order('created_at DESC').page(params[:page])
    
    respond_to do |format|  
      format.html # index.html.erb
      format.json { render json: @lists }
    end
  end
  # GET /lists
  # GET /lists.json
  def index
    @lists = current_user.lists.includes(:list_items, :users, :tag).order('created_at DESC').page(params[:page]).per(10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lists }
    end
  end

  # GET /lists/1
  # GET /lists/1.json
  def show
    @list = current_user.lists.where(:id => params[:id]).includes(:list_items, :users).first
    respond_to do |format|
      format.html { redirect_to list_urls_url(@list)}
      format.json { render json: @list }
    end
  end

  # GET /lists/new
  # GET /lists/new.json
  def new
    @list = List.new
    @tag = @list.build_tag
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @list }
    end
  end

  # GET /lists/1/edit
  def edit
    @list = List.find(params[:id])
    @tag = @list.tag || @list.build_tag
  end

  # POST /lists
  # POST /lists.json
  def create         
    @list = List.new(params[:list])
    @list.users.push(current_user)
    @list.creator = current_user
    @list.moderated = true if params[:moderated].to_s == "1"

    respond_to do |format|
      if @list.save               
        @list.create_unmoderated_membership(current_user)         
        flash[:notice] = "Your List was successfully created."
        format.html { redirect_to lists_url }
        format.json { render json: @list, status: :created, location: @list }
      else
        @tag = @list.tag || @list.build_tag
        format.html { render action: "new" }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lists/1
  # PUT /lists/1.json
  def update
    @list = List.find(params[:id])
    @tag = @list.tag || @list.build_tag
    respond_to do |format|
      if @list.update_attributes(params[:list])
        format.html { redirect_to lists_url, notice: 'List was successfully updated.' }
        format.json { head :no_content }
      else
       @tag = @list.tag || @list.build_tag
        format.html { render action: "edit" }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lists/1
  # DELETE /lists/1.json
  def destroy
    @list = List.find(params[:id])     
    respond_to do |format|
      if (@list.creator == current_user || current_user.is_admin?) && @list.users.size == 1       
        flash[:notice] = "Your list has been successfully removed."
        @list.destroy     
          format.html { redirect_to lists_url }
          format.json { head :no_content }
      else
         flash[:notice] = "This list cannot be deleted. There are other members who are part of this list."
         format.html { redirect_to lists_url }
         format.json { head :no_content }
      end
    end
  end
  def up_vote
    rating = Rating.up_vote(params[:id],@user.id)
    render json:{"error"=>rating[1],"vote"=>rating[2]}.to_json
 
  end
  def down_vote
    rating=Rating.down_vote(params[:id],@user.id)
    render json:{"error"=>rating[1],"vote"=>rating[2]}.to_json
  end
  
  private

  def set_user
    @user = current_user
  end
  
end
