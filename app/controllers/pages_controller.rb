class PagesController < ApplicationController
  before_filter :require_user
  
  def about_us
  end

  def contact_us
  end

  def terms_of_service
  end

end