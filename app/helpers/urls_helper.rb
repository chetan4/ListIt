module UrlsHelper

  def preview_image_for(url)
    image_url = ""
    if url.image_url.present?
      image_url = url.image_url
    else
      image_url = "/assets/no_image.png"
    end
    image_tag image_url, :size => '115x115'
  end
end
