module ListsHelper
  def up_vote_text(list)
    "Up vote(#{list.get_total_upvote})"
  end
  
  def down_vote_text(list)
    "Down vote(#{list.get_total_downvote})"
  end
end
