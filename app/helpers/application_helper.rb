
module ApplicationHelper           
  
  def action_status(list)
    status = ""
    status = "" if list.members.include?(current_user)
    status = "(Pending Approval)" if list.request_pending_members(current_user).present?
    status
  end
        
  def notifications_exist?
    (notification_size > 0)
  end
  
  def notification_size
    UserList.requested(current_user).size
  end

 def flash_class(name)
   if name.to_s=="notice" 
     "success"
   elsif name.to_s == "info"
     "info"
   else   
    "error" 
   end 
   
 end
 def active_class
   classes = {
     'home' => 'home',
     "lists" =>'lists',
     'users.show' => 'profile',
     'members.manage' => 'notifications',
     'projects' => 'projects'
     }

	  classes[controller.controller_name + '.' + controller.action_name] || classes[controller.controller_name] || ''
 end		

  def active_nav_class(class_name)  
	 active_class == class_name ? "active" : ""
	end
	
	def is_active?(controller, action)   
	 return 'active' if params[:controller].to_sym == controller && params[:action].to_sym == action     
  end
  
  def has_error?(object, attribute)                      
    object.errors[attribute.to_sym].present? ? "error" : ""    
  end

  def has_attachment_error?(object, attribute)
    errors = attachment_errors(object, attribute)
    errors.present? ? "error" : ""
  end

  def attachment_errors(object, attribute)
    errors=[]
    fields = ["#{attribute}_file_name", "#{attribute}_file_size", "#{attribute}_content_type", "#{attribute}_updated_at"]
    fields.each do|field|
      errors << object.errors[field.to_sym]
    end
    errors.reject{|error|!error.present?}
  end

  def sharethis(list)
    link_to "", "#", :class => "st_sharethis", :st_title => list.title, 
                                               :st_summary => list.description,
                                               :st_url => list_urls_url(list)
  end

  def subscribe_actions(list)
    if current_user
      if !current_user.subscribed?(list)
        link_to "Subscribe", add_subscription_user_path(current_user, :list_id => list.id), :method => :post, :remote => true
      else
        link_to "Unsubscribe", remove_subscription_user_path(current_user, :list_id => list.id), 
          :method => :post, :confirm => "Are you sure to unsunscribe this list?", :remote => true, :class => 'red-link'
      end
    end
  end
end
