
class MailmanWorker
 @queue = :mailman_queue
  def self.perform(message)
    MailmanManager.receive_email(Mail::Message.new(message))
  end
 
end  
