class SubscriptionNotifier
  @queue = :subscription_queue
  def self.perform(list_id, time_tracker)
    list = List.find(list_id)
    starts_at = time_tracker["starts_at"].to_datetime
    closes_at = time_tracker["closes_at"].to_datetime
    list_urls =  Url.where("created_at > ? AND created_at < ? AND list_id = ?", starts_at, closes_at, list.id)
    if !list_urls.blank?
      Notifier.for_subscription(list, list.subscribers, list_urls).deliver
    end
  end
end
