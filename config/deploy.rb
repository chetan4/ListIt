set :stages, %(staging, production)
set :default_stage, "production"
set :application, "firelist"
set :repository,  "git@github.com:idyllicsoftware/ListIt.git"
set :branch, "master" 
set :deploy_to, "/home/sid/apps/#{application}/"
$:.unshift(File.expand_path('./lib', ENV['rvm_path']))

require 'rvm/capistrano'
#require "whenever/capistrano"  
require 'bundler/capistrano'

set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

role :web, "ec2-107-22-250-41.compute-1.amazonaws.com"                          # Your HTTP server, Apache/etc
role :app, "ec2-107-22-250-41.compute-1.amazonaws.com"                          # This may be the same as your `Web` server
role :db,  "ec2-107-22-250-41.compute-1.amazonaws.com", :primary => true        # This is where Rails migrations will run
#role :db,  "your slave db-server here" 

set :keep_releases, 5   
set :use_sudo, false
set :user, "sid" 
set :rvm_type, :user      
set :rvm_ruby_string, "ruby-1.9.2-p180"
set :deploy_via, :remote_cache 

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
  
  desc "task to create a symlink for the database files."
  task :copy_database_yml, :roles => :app do
      run "ln -s #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end     
end    

namespace "whenever" do
  task :update, :roles => :app do                       
    if rails_env=='production'
      run "cd #{current_path}; crontab -r; whenever --update-crontab tasks --set environment=production"
    end
  end
end
     

before "deploy:assets:precompile", "deploy:copy_database_yml" 
