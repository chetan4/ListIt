# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

if environment=='development'
  #These settings depend on the machine/os, please kindly add your settings here about rvm, gem
  #TODO Need to make it more useful

  rvm_environment_specifier = "/home/jinesh/.rvm/bin/ruby-1.9.2-p290@listit" # Edit this line as per your machine rvm settings
  rake = "/home/jinesh/.rvm/gems/ruby-1.9.2-p290@global/bin/rake" # Edit this line as per your machine rvm settings

  job_type :rake, "cd :path && #{rvm_environment_specifier} #{rake} RAILS_ENV=:environment :task --silent :output"

else
  # production code depends on machine
end


set :output, "log/cron_log.log"

every 1.day, :at => '08:00 pm' do 
  rake "send_notifications"
end

