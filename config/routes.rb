ListIt::Application.routes.draw do

  resources :subscriptions

  resources :user_sessions 

  match '/auth/twitter/callback' => "user_sessions#twitter_auth"  

  match 'login' => "user_sessions#new",      :as => :login
  match 'logout' => "user_sessions#destroy", :as => :logout

  match 'activate(/:activation_code)' => 'users#activate', :as => :activate_account
  match 'send_activation(/:user_id)' => 'users#send_activation', :as => :send_activation

  match 'forgot_password' => 'user_sessions#forgot_password', :as => :forgot_password, :via => :get
  match 'forgot_password' => 'user_sessions#forgot_password_lookup_email', :as => :forgot_password, :via => :post

  put 'reset_password/:reset_password_code' => 'users#reset_password_submit', :as => :reset_password, :via => :put
  get 'reset_password/:reset_password_code' => 'users#reset_password', :as => :reset_password, :via => :get  

  resources :users do
    member do
      post :add_subscription
      post :remove_subscription 
    end   
    
    resources :lists, :only => [] do
      collection do
        get :peek
      end
    end
  end

  resources :projects

  resource :user, :as => 'account'  # a convenience route

  match 'signup' => 'users#new', :as => :signup
  resources :lists do 
    collection do
      get "all" 
      get "search"
    end  
    member do 
      get 'up_vote'
      get 'down_vote'
    end

    resources :urls do
      collection do
        get "search"
        
      end
      member do 
      post "add_comment"
      end
    end

    resources :members  do 
      member do 
        delete "reject"
        
      end     
    end
  end
 
  
  match "members/manage" => "members#manage", :via => :get, :as => "manage_members"
  match 'fetch_url_content' => 'urls#fetch_url_content'
  match 'profile' => 'users#edit_profile'
  match 'about_us' => 'pages#about_us'
  match 'contact_us' => 'pages#contact_us'
  match 'terms_of_service' => 'pages#terms_of_service'
  match '/admin' => 'admin::users#index'
  namespace :admin do 
    match '/users' => "users#index" 
    match '/users/:id' => "users#show"
    match '/users/destroy/:id' => "users#destroy",:as => :delete_user,:via => [:delete]
    match '/users/deactivate/:id' => "users#deactivate",:as => :deactivate_user
    match '/users/activate/:id' => "users#activate",:as => :activate_user
    match '/lists/:user_id' => "lists#index",:as=>:user_list
    match '/lists/destroy/:id' => "lists#destroy",:as => :destroy_list,:via => [:delete]
  end
  #resources :list_items
  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'user_sessions#new' 

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
