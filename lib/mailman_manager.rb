class MailmanManager  
  attr_accessor :user, :message, :subject, :from, :body  

  def initialize(message)
    @message = message
    @subject = message.subject
    @from = message.from
    @body = message.body    
  end
  
  def create_list_and_urls
    if user_exists?(@from.first) 
      puts "-- User exists --"
     List.transaction do
       create_list
     end
    end
  end
  
  def self.receive_email(message) 
    mailman_manager =  MailmanManager.new(message)
    mailman_manager.create_list_and_urls
  end 
  
  private 
  
  def user_exists?(email)
    @user = User.where(:email => email, :active =>  true).first
    (@user.present? ? true : false)
  end    
  
  def create_list    
    if list_exists?      
      create_url                    
    else                         
      @list = @user.lists.build(title: @subject, description: "update later")
      @list.moderated = true
      @list.creator = @user   
      @list.users.push(@user)                   
      if @list.save!
        @list.create_unmoderated_membership(@user) 
        create_url
        @list.solr_index!        
      end
    end
  end
  
  def create_url

    split_message = message.text_part.to_s.split(' ') 
    split_message = message.body.to_s.split(' ') if  split_message.empty?
    split_message.each do |msg|
      begin
        url_data = UrlData.new(msg)

        unless url_data.errors.present?
          url = @list.urls.new(:url => msg, :title => url_data.title.first ||url_data.url, :description => url_data.description.first)

          url.build_tag(:keywords=>url_data.keywords.first) if url_data.keywords.first.present?
          
          url.creator = @user
          if !url.valid?          
            next
          else
            url.save!
        end
        else
          next
        end  
      
     rescue
     end    
    end    
  end 
  
  def list_exists?
    @list = @user.lists.where(title: @subject).first
    puts "List present: #{@list.present?}"
    @list.present?
  end
end
