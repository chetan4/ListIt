require 'nokogiri'
require 'open-uri'

module ActiveRecord
  module ContentGrabber                         
    def self.included(base)
      base.extend(ClassMethods)
      base.send(:include, InstanceMethods)
    end
    
    module InstanceMethods     
      def get_url_contents(url)
        @doc = Nokogiri::HTML(open(url))        
      rescue => e                                                          
        Rails.logger.info("[Error]: #{e.message} \n #{e.backtrace}")
        nil
      end
      
      def get_site_title
        title_response = []   
        @doc.search('title').each do |title|  
          title_response.push(title.content.to_s.gsub(/\r\n/m,'').to_s)
        end                   
        
        title_response
      end    
      
      def get_site_description
        description_responses = []        
        valid_description_labels = ['description', 'content', 'og:description']
        valid_description_labels.each do |label|
          @doc.search('meta').each do |meta_tag|                     
            description_responses.push(meta_tag.attributes["content"].to_s.gsub(/\r\n/m,'').to_s) if meta_tag.attributes["property"].to_s == label.to_s || (meta_tag.attributes["name"] && meta_tag.attributes["name"].to_s.downcase == label.to_s)
          end
        end                  
        
        description_responses
      end
      
      def get_site_images
        image_response = []
        @doc.search('meta').each do |meta_tag|
          image_response.push(meta_tag.attributes['content'].to_s) if meta_tag.attributes["property"].to_s == 'og:image' || (meta_tag.attributes["name"] && meta_tag.attributes["name"].to_s.downcase == 'og:image')
        end           
                                                                                                             
        image_response
      end 
      
      def get_site_keywords
        keyword_response = []
        @doc.search('meta').each do |meta_tag|
          keyword_response.push(meta_tag.attributes['content'].to_s) if meta_tag.attributes["property"].to_s == 'keywords' || (meta_tag.attributes["name"] && meta_tag.attributes["name"].to_s.downcase == 'keywords')
        end           
                                                                                                             
        keyword_response
      end    
    end

    module ClassMethods      
      def grab_url_contents(url)                     
        response_hash = {}
        klass_instance = self.allocate
        klass_instance.get_url_contents(url)
        response_hash[:titles] = klass_instance.get_site_title
        response_hash[:description] = klass_instance.get_site_description 
        response_hash[:images] = klass_instance.get_site_images
        response_hash[:keywords] = klass_instance.get_site_keywords        
        response_hash
      end
    end    
  end     
end

ActiveRecord::Base.send(:include, ActiveRecord::ContentGrabber)
ActionController::Base.send(:include, ActiveRecord::ContentGrabber)
