task :send_notifications => :environment do
  closes_at = Time.now
  starts_at = closes_at - 1.day
  urls_to_be_sent =  Url.where("created_at > ? AND created_at < ?", starts_at, closes_at).includes(:list)
  lists = urls_to_be_sent.collect(&:list).uniq
  lists.each do |list|
   Resque.enqueue(SubscriptionNotifier, list.id, {:starts_at => starts_at, :closes_at => closes_at})
  end
end
